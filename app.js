// UI Variables
const currencyOneUI = document.getElementById("currency-one");
const amountOneUI = document.getElementById("amount-one");
const currencyTwoUI = document.getElementById("currency-two");
const amountTwoUI = document.getElementById("amount-two");
const swapUI = document.getElementById("swap");
const rateUI = document.getElementById("rate");

// Event Listner
currencyOneUI.addEventListener("change", calculateRate);
amountOneUI.addEventListener("input", calculateRate);
currencyTwoUI.addEventListener("change", calculateRate);
amountTwoUI.addEventListener("input", calculateRate);
swapUI.addEventListener("click", () => {
    const currencyOne = currencyOneUI.value;
    currencyOneUI.value = currencyTwoUI.value ;
    currencyTwoUI.value = currencyOne;
    calculateRate();
});

// Calculate Exchange Rate
function calculateRate() {
    const currencyOne = currencyOneUI.value;
    const currencyTwo = currencyTwoUI.value;
    fetch(`https://api.exchangerate-api.com/v4/latest/${currencyOne}`)
        .then(res => res.json())
        .then(data => {
            const rate = data.rates[currencyTwo];
            rateUI.innerText = `1 ${currencyOne} = ${rate} ${currencyTwo}`;
            amountTwoUI.value = (amountOneUI.value * rate).toFixed(3);
            // console.log(currencyOne, rate);
        })
}

// calculateRate()

// async / await
async function getApiData() {
    const currencyOne = currencyOneUI.value;
    const currencyTwo = currencyTwoUI.value;
    const request = await fetch(`https://api.exchangerate-api.com/v4/latest/${currencyOne}`);
    const resData = await request.json();
    const rates = resData.rates;
    let output1 = "";
    let output2 = "";
    for (const key in rates) {
        if (rates.hasOwnProperty.call(rates, key)) {
            const element = key;
            if(element === "USD") {
                output1 += `<option value="${element}" selected>${element}</option>`
            } else {
                output1 += `<option value="${element}">${element}</option>`
            }
            
        }
    }
    for(const key in rates) {
        if(key === "EUR") {
            output2 += `<option value="${key}" selected>${key}</option>`
        } else {
            output2 += `<option value="${key}">${key}</option>`
        }
    }
    currencyOneUI.innerHTML = output1;
    currencyTwoUI.innerHTML = output2;    
    calculateRate()
    
}

getApiData()